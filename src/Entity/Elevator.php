<?php

namespace App\Entity;

use App\Repository\ElevatorRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ElevatorRepository::class)
 */
class Elevator
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $currentFloor;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $numberFloorsTraveled;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $pendingTravels = [];

    /**
     * Elevator constructor.
     * @param int $id
     * @param int $currentFloor
     */
    public function __construct(int $id, int $currentFloor)
    {
        $this->id = $id;
        $this->currentFloor = $currentFloor;
        $this->numberFloorsTraveled = 0;
        $this->pendingTravels = [];
    }

    /**
     * @param int $id
     * @param int $currentFloor
     * @return static
     */
    public static function create(int $id, int $currentFloor): self
    {
        return new self($id, $currentFloor);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getCurrentFloor(): int
    {
        return $this->currentFloor;
    }

    /**
     * @return int
     */
    public function getNumberFloorsTraveled(): int
    {
        return $this->numberFloorsTraveled;
    }

    public function getNumberOfPendingTravels(): int
    {
        return count($this->pendingTravels);
    }

    public function addTravel($floor): void
    {
        $this->pendingTravels [] = $floor;
    }

    public function getLastPendingTravel()
    {
        return end($this->pendingTravels);
    }

    public function runNextTravel(): void
    {
        if (count($this->pendingTravels) > 0) {
            $this->currentFloor = $this->pendingTravels[0];
            $this->numberFloorsTraveled++;
            array_shift($this->pendingTravels);
        }
    }
}
