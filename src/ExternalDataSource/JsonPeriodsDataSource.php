<?php


namespace App\ExternalDataSource;

use App\Entity\Period;

class JsonPeriodsDataSource
{
    private $periods;

    public function __construct($sources){
        $this->periods = [];
        foreach ($sources as $source){
            if (!file_exists($source)) {
                throw new \Exception("json file with periods data doesn't exist");
            }
            $infoPeriods = json_decode(file_get_contents($source));
            foreach($infoPeriods as $infoPeriod){
                $this->periods[] = Period::create(
                    new \DateTime($infoPeriod->initPeriod),
                    new \DateTime($infoPeriod->endPeriod),
                    $infoPeriod->frequencyCalls,
                    $infoPeriod->fromFloor,
                    $infoPeriod->toFloor
                );
            }
        }
    }

    public function getData()
    {
        return $this->periods;
    }
}