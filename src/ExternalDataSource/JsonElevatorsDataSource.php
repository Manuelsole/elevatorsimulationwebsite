<?php


namespace App\ExternalDataSource;

use App\Entity\Elevator;

class JsonElevatorsDataSource
{
    private $elevators;

    public function __construct($sources){
        $this->elevators = [];
        foreach ($sources as $source){
            if (!file_exists($source)) {
                throw new \Exception("json file with elevators data doesn't exist");
            }
            $infoElevators = json_decode(file_get_contents($source));
            foreach($infoElevators as $infoElevator){
                $this->elevators[] = Elevator::create(
                    $infoElevator->id,
                    $infoElevator->currentFloor
                );
            }
        }
    }

    public function getData(): array
    {
        return $this->elevators;
    }
}