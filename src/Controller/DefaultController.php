<?php
namespace App\Controller;

use App\ExternalDataSource\JsonElevatorsDataSource;
use App\Service\Simulation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{

    private $simulationService;

    public function __construct(Simulation $simulationService)
    {
        $this->simulationService = $simulationService;
    }

    public function index(): Response
    {
        $this->simulationService->run();

        return $this->render('default/index.html.twig', [
            'report' => $this->simulationService->getReport()
        ]);
    }
}