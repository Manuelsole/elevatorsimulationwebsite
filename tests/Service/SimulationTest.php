<?php

namespace App\Tests\Service;

use App\ExternalDataSource\JsonElevatorsDataSource;
use App\ExternalDataSource\JsonPeriodsDataSource;
use App\Service\Simulation;
use PHPUnit\Framework\TestCase;
use stdClass;

class SimulationTest extends TestCase
{

    public function testInitDateGreaterThanEndDate()
    {
        $elevators = new JsonElevatorsDataSource([dirname(__FILE__) . '/../datasource/elevators.json']);
        $periods = new JsonPeriodsDataSource([dirname(__FILE__) . '/../datasource/periods.json']);
        $initDate = ['2021/05/02 20:00:00'];
        $endDate = ['2021/05/02 09:00:00'];
        $this->expectException(\Exception::class);
        try {
            new Simulation($initDate, $endDate, $elevators, $periods);
        } catch (\Exception $e) {
            $this->assertEquals($e->getMessage(), "Simulation initial date can't be greater than end date");
            throw $e;
        }

    }
}
