<?php

namespace App\Tests\ExternalDataSource;

use App\ExternalDataSource\JsonElevatorsDataSource;

use PHPUnit\Framework\TestCase;

class JsonElevatorsDataSourceTest extends TestCase
{
    public function testEmptyPath(){
        $arrayPath = [''];
        $this->expectException(\Exception::class);
        try {
            new JsonElevatorsDataSource($arrayPath);
        } catch (\Exception $e) {
            $this->assertEquals($e->getMessage(), "json file with elevators data doesn't exist");
            throw $e;
        }
    }

    public function testExatlyNumberOfElevatorsThatInSourceFile(){
        $JsonElevatorsDataSourceService = new JsonElevatorsDataSource([dirname(__FILE__) . '/../datasource/elevators.json']);
        $elevators = $JsonElevatorsDataSourceService->getData();
        $this->assertEquals(3, count($elevators));
    }
}
