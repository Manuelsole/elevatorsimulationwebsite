Elevator Simulation Website
===========================

Instructions:

1. Clone repo in you system with:
   
	**git clone https://bitbucket.org/Manuelsole/elevatorsimulationwebsite.git**


2. You will need have composer installed in your system. You can find it in this direction:
   https://getcomposer.org/

   
3. After install composer go to root folder of project and write this command: 
   **composer install**
   

2. For display this website you need install the symfony client first. You can find it in this direction:
https://symfony.com/download


3. After install symfony client you can start local server. First go to root folder of project and then execute this command:
**symfony server:start**

    If all works fine, this command show you a message like this:
 

      [OK] Web server listening
      
      The Web server is using PHP CGI 7.4.9
      
      http://127.0.0.1:8000
	  

4. Now you can go to your browser and write this url:
**http://127.0.0.1:8000**


5. This symfony app will show you the results of simulation in his homepage.